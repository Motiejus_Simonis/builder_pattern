package services.helpers;

import models.bs.Player;
import models.cons.Gender;
import services.RaceBuilder;

public class PlayerDirector {
    private RaceBuilder raceBuilder;

    public PlayerDirector(RaceBuilder newRaceBuilder) {
        raceBuilder = newRaceBuilder;
    }

    public PlayerDirector construct(String name, Gender gender) {
        raceBuilder.setNameAndGender(name, gender);
        raceBuilder.setPresets();
        return this;
    }

    public Player getPlayer(){
        return raceBuilder.getResult();
    }
}

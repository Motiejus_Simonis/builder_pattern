package services.impl;

import models.bs.Items;
import models.bs.Player;
import models.cons.Armor;
import models.cons.Gender;
import models.cons.Race;
import models.cons.Weapon;
import services.RaceBuilder;
import java.util.Arrays;

public class OrcRaceBuilder implements RaceBuilder {

    private Player player;

    @Override
    public void setNameAndGender(String name, Gender gender) {
        player = new Player(name, gender);
    }

    @Override
    public void setPresets() {
        player.setRace(Race.ORC);
        //Setting starting items
        player.setItems(new Items(Arrays.asList(Weapon.AXE), Arrays.asList(Armor.MAIL), null));
    }

    @Override
    public Player getResult() {
        return player;
    }
}

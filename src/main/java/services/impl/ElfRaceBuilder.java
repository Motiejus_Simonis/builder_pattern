package services.impl;

import models.bs.Items;
import models.bs.Player;
import models.cons.Armor;
import models.cons.Gender;
import models.cons.Race;
import models.cons.Weapon;
import services.RaceBuilder;
import java.util.Arrays;

public class ElfRaceBuilder implements RaceBuilder {

    private Player player;

    @Override
    public void setNameAndGender(String name, Gender gender) {
        player = new Player(name, gender);
    }

    @Override
    public void setPresets() {
        player.setRace(Race.ELF);
        //Setting starting items
        player.setItems(new Items(Arrays.asList(Weapon.BOW), Arrays.asList(Armor.BOILED_LEATHER), null));
    }

    @Override
    public Player getResult() {
        return player;
    }
}

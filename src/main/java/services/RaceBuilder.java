package services;

import models.bs.Player;
import models.cons.Gender;

public interface RaceBuilder {

    void setNameAndGender(String name, Gender gender);
    void setPresets();
    Player getResult();
}

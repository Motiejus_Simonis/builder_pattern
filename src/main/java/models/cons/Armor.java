package models.cons;

public enum Armor {
    MAIL, BOILED_LEATHER, PLATE;
}

package models.bs;

import lombok.Getter;
import lombok.Setter;
import models.cons.Armor;
import models.cons.Potion;
import models.cons.Weapon;

import java.util.List;

@Getter
@Setter
public class Items {

    private List<Weapon> weapons;
    private List<Armor> armors;
    private List<Potion> potions;

    public Items(List<Weapon> newWeapons, List<Armor> newArmors, List<Potion> newPotions) {
        weapons = newWeapons;
        armors = newArmors;
        potions = newPotions;
    }
}

package models.bs;

import lombok.Getter;
import lombok.Setter;
import models.cons.Gender;
import models.cons.Race;

@Getter
@Setter
public class Player {

    private String name;
    private Race race;
    private Gender gender;
    private Items items;

    public Player(String newName, Gender newGender) {
        name = newName;
        gender = newGender;
    }
}

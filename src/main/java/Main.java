import models.bs.Player;
import models.cons.Gender;
import services.helpers.PlayerDirector;
import services.impl.HumanRaceBuilder;
import services.impl.OrcRaceBuilder;


/**
 * BUILDER DESIGN PATTERN
 * Intent:
    Separate the construction of a complex object from its representation so that the same construction process can create different representations.
    Parse a complex representation, create one of several targets.
 */
public class Main {
    public static void main(String[] args) {

        Player p1 = new PlayerDirector(new OrcRaceBuilder()).construct("Oghul", Gender.MEN).getPlayer();
        Player p2 = new PlayerDirector(new HumanRaceBuilder()).construct("Oksana", Gender.WOMEN).getPlayer();

        printPlayerStatistics(p1);
        printPlayerStatistics(p2);
    }

    private static void printPlayerStatistics(Player player) {
        System.out.println("Player name - " + player.getName() + ", race - " + player.getRace() + ", gender - " + player.getGender());
    }
}
